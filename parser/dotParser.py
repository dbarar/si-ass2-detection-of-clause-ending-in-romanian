import sys
import csv
from datetime import datetime
prescurtari_data = ''
prescurtati_file_name = "prescurtari.txt"
csv_file_name = ""
features_name = ['predAbr', 'ora', 'data', 'numar', 'predPresc', 'web', 'nume', 'urmEOL', 'sfarsitDeProp']
features_data = []


def parse_line(line):
    global features_data
    splited_line = line.split()

    splited_line_size = len(splited_line)
    for i in range(splited_line_size):
        word = splited_line[i]
        word_size = len(word)
        for j in range(word_size):
            char = word[j]
            if char == '.':
                data, numar, nume, ora, pred_abr, pred_presc, sfarsit_de_prop, urm_eol, web = init_features_data_line()
                features_data_line = []
                prec_word = eliminateComma(word)
                prec_word = eliminateParanthesis(prec_word)

                if i != splited_line_size - 1: next_word = splited_line[i + 1]

                pred_presc = set_pred_presc(prec_word)
                pred_abr = set_pred_abr(prec_word)
                web = set_web(prec_word)
                data = set_data(prec_word)
                ora = set_ora(prec_word)
                numar = set_numar(prec_word, data, ora)

                pred_abr, ora, data, numar, pred_presc, web, nume, sfarsit_de_prop, urm_eol = \
                    set_eof(i, j, prec_word, pred_abr, ora, data, numar, pred_presc, web, nume,
                            sfarsit_de_prop, splited_line_size, urm_eol)

                sfarsit_de_prop, data, numar, nume, ora, web, prec_word, pred_abr, pred_presc = \
                    set_sfarsit_de_prof(j, data, numar, nume, ora, web, prec_word, pred_abr,
                                        pred_presc, sfarsit_de_prop)

                #features_data_line.append(prec_word)
                set_files_of_feature_for_a_dot(data, features_data_line, numar, nume, ora, pred_abr, pred_presc,
                                               sfarsit_de_prop, urm_eol, web)
                features_data.append(features_data_line)
                print(features_data_line)


def set_numar(prec_word, data, ora):
    prec_word = eliminate_parant_dot(prec_word)
    numar = 'no'
    if prec_word[len(prec_word) - 1] == '%':
        prec_word = prec_word[0:len(prec_word) - 1]
    if data == 'no' and ora == 'no':
        try:
            string = prec_word.replace(".", "")
            string = string.replace(",", ".")
            string = float(string)
            numar = 'yes'
        except:
            numar = 'no'
    return numar


def eliminate_parant_dot(prec_word):
    if prec_word.endswith(")."):
        prec_word = prec_word[0:len(prec_word) - 2]
    return prec_word


def set_ora(prec_word):
    prec_word = eliminate_parant_dot(prec_word)
    try:
        datetime.strptime(prec_word, '%H.%M')
        ora = 'yes'
    except:
        ora = 'no'
    return ora


def set_data(prec_word):
    prec_word = eliminate_parant_dot(prec_word)
    try:
        datetime.strptime(prec_word, '%d.%m.%Y')
        data = 'yes'
    except:
        data = 'no'
    return data


def set_sfarsit_de_prof(dot_position, data, numar, nume, ora, web, prec_word, pred_abr, pred_presc, sfarsit_de_prop):
    if sfarsit_de_prop == 'no':
        if data == 'yes' or numar == 'yes' or ora == 'yes' or web == 'yes':
            if dot_position == len(prec_word) - 1:
                data, numar, nume, ora, pred_abr, pred_presc, sfarsit_de_prop, urm_eol, web = init_features_data_line()
                sfarsit_de_prop = 'yes'

        elif data == 'no' and numar == 'no' and nume == 'no' and ora == 'no' and pred_presc == 'no' and pred_abr == 'no':
            if dot_position == len(prec_word) - 1:
                sfarsit_de_prop = 'yes'
            else:
                for i in range(dot_position + 1, len(prec_word)):
                    if prec_word[i] in "]})":
                        sfarsit_de_prop = 'yes'
    return sfarsit_de_prop, data, numar, nume, ora, web, prec_word, pred_abr, pred_presc


def set_web(prec_word):
    prec_word = eliminate_parant_dot(prec_word)
    web = 'no'
    if "www." in prec_word or "http." in prec_word or "https." in prec_word:
        web = 'yes'
    return web


def set_pred_abr(prec_word):
    prec_word = eliminate_parant_dot(prec_word)
    pred_abr = 'no'
    count_of_dots = prec_word.count('.')
    if len(prec_word) == 2 * count_of_dots:
        if prec_word == prec_word.upper():
            pred_abr = 'yes'
    return pred_abr


def set_pred_presc(prec_word):
    prec_word = eliminate_parant_dot(prec_word)
    pred_presc = 'no'
    global prescurtari_data
    if prec_word in prescurtari_data:
        pred_presc = 'yes'
    return pred_presc


def set_eof(i, j, prec_word, pred_abr, ora, data, numar, pred_presc, web, nume, sfarsit_de_prop, splited_line_size, urm_eol):
    if i == splited_line_size - 1:
        if j == len(prec_word) - 1:
            urm_eol = 'yes'
            sfarsit_de_prop = 'yes'
            pred_abr = 'no'
            ora = 'no'
            data = 'no'
            numar = 'no'
            pred_presc = 'no'
            web = 'no'
            nume = 'no'
    return pred_abr, ora, data, numar, pred_presc, web, nume, sfarsit_de_prop, urm_eol


def eliminateComma(prec_word):
    if prec_word[len(prec_word) - 1] == ',':
        prec_word = prec_word[0:len(prec_word) - 1]
    return prec_word


def eliminateParanthesis(prec_word):
    if prec_word[0] in '([{':
        prec_word = prec_word[1:]
    if prec_word[len(prec_word) - 1] in ')]}':
        prec_word = prec_word[0:len(prec_word) - 1]
    return prec_word


def init_features_data_line():
    pred_abr = 'no'
    ora = 'no'
    data = 'no'
    numar = 'no'
    pred_presc = 'no'
    web = 'no'
    nume = 'no'
    urm_eol = 'no'
    sfarsit_de_prop = 'no'
    return data, numar, nume, ora, pred_abr, pred_presc, sfarsit_de_prop, urm_eol, web


def set_files_of_feature_for_a_dot(data, features_data_line, numar, nume, ora, pred_abr, pred_presc, sfarsit_de_prop,
                                   urm_eol, web):
    features_data_line.append(pred_abr)
    features_data_line.append(ora)
    features_data_line.append(data)
    features_data_line.append(numar)
    features_data_line.append(pred_presc)
    features_data_line.append(web)
    features_data_line.append(nume)
    features_data_line.append(urm_eol)
    features_data_line.append(sfarsit_de_prop)


def write_results():
    global csv_file_name
    global features_data
    with open(csv_file_name, "w") as csv_file:
        spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        spam_writer.writerow(features_name)
        for data in features_data:
            spam_writer.writerow(data)


def read_prescurtari():
    global prescurtari_data
    global prescurtati_file_name
    with open(prescurtati_file_name) as f:
        for line in f:
            prescurtari_data += line


def read_entry_text(name_file):
    read_data = ''
    with open(name_file, encoding='utf8') as f: #
        for line in f:
            read_data += line
            parse_line(line)
    print(read_data)


def main():
    global csv_file_name
    print(features_name)
    name_file = sys.argv[1]
    csv_file_name = name_file.split('.')[0] + "Results.csv"
    read_prescurtari()
    read_entry_text(name_file)
    write_results()


if __name__ == '__main__':
    main()
